const mainNav = document.querySelector(".main-nav");
const bar = document.querySelector(".bar-sub-container");
const close = document.querySelector(".close-sub-container");
const closeOpenContainer = document.querySelector(".close-open-container");
const navLinks = document.querySelectorAll(".nav-link");

closeOpenContainer.addEventListener("click", function () {
  mainNav.classList.toggle("nav-open");

  bar.classList.toggle("overlay");
  bar.classList.toggle("hidden");

  close.classList.toggle("overlay");
  close.classList.toggle("hidden");
});

navLinks.forEach((navLink) => {
  navLink.addEventListener("click", function () {
    mainNav.classList.toggle("nav-open");

    bar.classList.toggle("overlay");
    bar.classList.toggle("hidden");

    close.classList.toggle("overlay");
    close.classList.toggle("hidden");
  });
});

// STICKY NAVIGATION
const headerSection = document.querySelector(".header ");

const obs = new IntersectionObserver(
  function (entries) {
    const ent = entries[0];
    console.log(ent);

    if (ent.isIntersecting === false) {
      document.querySelector(".main-nav").classList.add("sticky");
    } else {
      document.querySelector(".main-nav").classList.remove("sticky");
    }
  },
  {
    root: null,
    threshold: 0,
  }
);
obs.observe(headerSection);
